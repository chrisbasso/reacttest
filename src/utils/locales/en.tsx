export default {
  DETAILS_gender: 'Gender',
  DETAILS_height: 'Height',
  DETAILS_mass: 'Mass',
  DETAILS_birth_year: 'Gender',
  DETAILS_eye_color: "Eye's color",

  MSG_LOGIN: "If you are not yet a member, ask for a promoter: From Bs.As. 0800-777-4253. From other Provinces 0810-999-4253",
  MSG_REGISTER: "If you have already signed in at www.e-galeno.com.ar you must log in with the same information. You do not have to sign in again. If you have not yet logged in, complete the following form.",

  MSG_MISSING_PASSWORD: "Insert a Password",
  MSG_MISSING_NUMBER: "Insert a Number",
  MSG_MISSING_CREDENTIAL: "Insert a valid Credential Number",
  MSG_MISSING_DOCUMENT: "Insert a valid Document Number",
  MSG_MISSING_PHONE: "Insert a Phone Number",
  MSG_INCORRECT_PASSWORDS: "Invalid Password",
  MSG_MISSING_EMAIL: "Insert an Email",
  MSG_INCORRECT_EMAILS: "Email Invalido",
  MSG_SUCCES_SEND_EMAIL: "Email sent successfully.",
  MSG_ERROR_SEND_EMAIL: "An error ocurred while sending the email.",


  CREDENTIAL_TYPE: 'Credential Type',
  LOGIN: "Log In",
  WELCOME_TO_LOGIN: "Welcome to the login screen!",
  EMAIL_PLACEHOLDER: "Email",
  CONFIRM_EMAIL_PLACEHOLDER: "Confirm Email",
  PASSWORD_PLACEHOLDER: "Password",
  CONFIRM_PASSWORD_PLACEHOLDER: "Confirm Password",
  NUMBER_PLACEHOLDER: "Number",
  CREDENTIAL_NUMBER: "Credential Number",
  DOCUMENT_NUMBER: "Credential Number",
  PHONE_PLACEHOLDER: "Phone Number",
  FORGOT_PASSWORD: "Forgot your Password",
  REGISTER: "Sign In",
  FORGOT_MY_PASSWORD: "Forgot my password",

  BTN_SEND: "Send",
  BTN_SIGNIN: "Sign In",

};

