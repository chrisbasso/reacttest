export default {
  DETAILS_gender: 'Genero',
  DETAILS_height: 'Altura',
  DETAILS_mass: 'Peso',
  DETAILS_birth_year: 'Nacimiento',
  DETAILS_eye_color: "Ojos",

  MSG_LOGIN: "Si aun no es socio, solicite un promotor: Desde Bs.As. 0800-777-4253. Desde el interior 0810-999-4253",
  MSG_REGISTER: "Si ya se registró en www.e-galeno.com.ar debe ingresar con los mismos datos. No debe registrarse nuevamente. Si todavía no se registró, Complete el siguiente formulario.",

  MSG_MISSING_PASSWORD: "Inserte una Contraseña",
  MSG_MISSING_NUMBER: "Inserte un Número",
  MSG_MISSING_CREDENTIAL: " Inserte un Número de Credencial válido",
  MSG_MISSING_DOCUMENT: " Inserte un Número de Documento Válido",
  MSG_MISSING_PHONE: " Inserte un  Número de Teléfono",
  MSG_INCORRECT_PASSWORDS: "Contraseña Inválida",
  MSG_INCORRECT_EMAILS: "Email Invalido",
  MSG_MISSING_EMAIL: " Inserte un Email",
  MSG_SUCCES_SEND_EMAIL: "Mail enviado correctamente",
  MSG_ERROR_SEND_EMAIL: "Ocurrió un error al enviar el mail.",


  CREDENTIAL_TYPE: 'Tipo de Credencial',
  LOGIN: "Ingresar",
  WELCOME_TO_LOGIN: "Bienvenidos a Galeno Afiliados!",
  EMAIL_PLACEHOLDER: "Email",
  CONFIRM_EMAIL_PLACEHOLDER: "Confirmar Email",
  PASSWORD_PLACEHOLDER: "Contraseña",
  CONFIRM_PASSWORD_PLACEHOLDER: "Confirmar Contraseña",
  NUMBER_PLACEHOLDER: "Número",
  CREDENTIAL_NUMBER: "N° Credencial",
  DOCUMENT_NUMBER: "N° Documento",
  FORGOT_PASSWORD: "Olvido su Contraseña",
  REGISTER: "Registración",
  PHONE_PLACEHOLDER: "N° Teléfono",
  FORGOT_MY_PASSWORD: "Olvidé mi contraseña",

  BTN_SEND: "Enviar",
  BTN_SIGNIN: "Registrate",

};
