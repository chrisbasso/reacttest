import en from './locales/en';
import es from './locales/es';
import { Localization } from 'expo';
import i18n from 'i18n-js';

i18n.fallbacks = true;
i18n.translations = { es, en };
i18n.locale = Localization.locale;

export default i18n;