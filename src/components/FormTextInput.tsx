import React from "react";
import { StyleSheet, TextInput, TextInputProps, View, Text } from "react-native";
import colors from "../utils/config/colors";


interface Props extends TextInputProps {
  label?: String | undefined
};
interface State {
  value: string;
}
class FormTextInput extends React.Component<Props, State> {
  render() {
    const { style, ...otherProps } = this.props;
    return (
      <View>
        {this.props.label ? <Text style={styles.inputColorPlaceholder}>{this.props.label}</Text> : null}
        <TextInput
          selectionColor={colors.DODGER_BLUE}
          style={[styles.textInput, style]}
          {...otherProps}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    borderColor: colors.SILVER,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginBottom: 20
  },
  inputColorPlaceholder: {
    color: 'gray'
  }
});


export default FormTextInput;