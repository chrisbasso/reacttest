import React from 'react';
import { Button} from 'react-native';
import PropTypes from 'prop-types';


const ButtonHelloWorld = (props: ButtonHelloWorld.propTypes) => (
  <Button onPress={props.callback} title={props.title}>
  </Button>
);

ButtonHelloWorld.propTypes = {
    title: PropTypes.string,
    callback: PropTypes.func
};

export default ButtonHelloWorld;