import React from "react";
import { StyleSheet, TextInput, TextInputProps, View, Text } from "react-native";
import colors from "../utils/config/colors";

interface Props extends TextInputProps {
  label?: String | undefined
};
class PasswordInput extends React.Component<Props> {
  textInputRef: TextInput | undefined | null;
  focus() {
    if (this.textInputRef)
      this.textInputRef.focus();
  }
  render() {
    const { style, ...otherProps } = this.props;

    return (
      <View>
        {this.props.label ? <Text style={styles.inputColorPlaceholder}>{this.props.label}</Text> : null}
        <TextInput ref={(input) => this.textInputRef = input}
          selectionColor={colors.DODGER_BLUE}
          style={[styles.textInput, style]}
          secureTextEntry={true}
          {...otherProps}
        />
      </View>

    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    borderColor: colors.SILVER,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginBottom: 20
  },
  inputColorPlaceholder: {
    color: 'gray'
  }
});

export default PasswordInput;