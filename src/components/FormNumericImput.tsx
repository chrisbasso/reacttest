import React from "react";
import { StyleSheet, TextInput, TextInputProps, Text, View } from "react-native";
import colors from "../utils/config/colors";

interface Props extends TextInputProps {
  label?: String | undefined
};
interface State {
  value: string;
}
const pattern = "  /\(([0-9])\)/";
class FormNumericInput extends React.Component<Props, State> {
  state: State = {
    value: ""
  }
  handleTextChange = (text: { nativeEvent: { text: string; }; }) => {
    let value = text.nativeEvent.text;
    console.log(value.replace(".", "").replace("-", ""))
    const condition = new RegExp(pattern);
    console.log(condition.test(value))
    // if (condition.test(value)) {
    this.setState({ value: value.replace(".", "").replace("-", "") })
    return true;
    // }
    console.log("State: " + this.state.value)
    return false;
  }
  textInputRef: TextInput | undefined | null;
  focus() {
    if (this.textInputRef)
      this.textInputRef.focus();
  }
  render() {
    const { style, ...otherProps } = this.props;
    return (
      <View>
        {this.props.label ? <Text style={styles.inputColorPlaceholder}>{this.props.label}</Text> : null}
        <TextInput ref={(input) => this.textInputRef = input}
          selectionColor={colors.DODGER_BLUE}
          style={[styles.textInput, style]}
          keyboardType={'number-pad'}
          onChange={event => this.handleTextChange(event)}
          value={this.state.value}
          {...otherProps}
        />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    borderColor: colors.SILVER,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginBottom: 20
  },
  inputColorPlaceholder: {
    color: 'gray'
  }
});

export default FormNumericInput;