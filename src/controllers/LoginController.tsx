export default class LoginController {
    static encriptarContraseña = (number, password, callback) => {

        fetch('http://www.e-galenomovil.com.ar/mobile-rest-services/encriptarHTMLService/' + password, {
            method: 'GET'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                LoginController.ingresar(number, responseJson.valor, callback);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    static ingresar = (number, encryptedPassword, callback) => {

        console.log(encryptedPassword)
        console.log(number)
        fetch('http://www.e-galenomovil.com.ar/mobile-rest-services/asociadoLogin/' + number + '/' + encryptedPassword + '/0', {
            method: 'GET'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                callback(responseJson)
            })
            .catch((error) => {
                console.error(error);
            });
    }
};