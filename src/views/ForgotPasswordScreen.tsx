import React from "react";
import { StyleSheet, View, KeyboardAvoidingView, TouchableOpacity, Alert } from "react-native";
import Button from "../components/Button";
import FormNumericInput from "../components/FormNumericImput";
import colors from "../utils/config/colors";
import i18n from '../utils/language';
import { Dropdown } from 'react-native-material-dropdown';
import ForgontPasswordController from '../controllers/ForgontPasswordController';

const CREDENTIAL_NUMBER: any = i18n.t('CREDENTIAL_NUMBER');
const DOCUMENT_NUMBER: any = i18n.t('DOCUMENT_NUMBER');
interface State {
    number: string,
    type: string
}
const data = [
    {
        value: CREDENTIAL_NUMBER
    }
    , {
        value: DOCUMENT_NUMBER
    }
]

export default class ForgotPasswordScreen extends React.Component<{ navigation: any }, State> {
    readonly state: State = {
        number: "",
        type: CREDENTIAL_NUMBER,
    };

    handleSendPress = () => {
        if (this.state.number.length > 0)
            ForgontPasswordController.send(this.state.number, (succes: boolean) => {
                if (succes) {
                    this.props.navigation.goBack()
                    Alert.alert(i18n.t('MSG_SUCCES_SEND_EMAIL'));
                }
                else
                    Alert.alert(i18n.t('MSG_ERROR_SEND_EMAIL'));
            })
        else
            Alert.alert(this.state.type === CREDENTIAL_NUMBER ? i18n.t('MSG_MISSING_CREDENTIAL') : i18n.t('MSG_MISSING_DOCUMENT'));
    };
    render() {

        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                <View style={styles.form}>
                    <Dropdown label={i18n.t('CREDENTIAL_TYPE')} data={data}
                        value={CREDENTIAL_NUMBER} onChangeText={(selection: string) => this.setState({ type: selection })} />
                    <FormNumericInput value={this.state.number}
                        onChangeText={(text) => this.setState({ number: text })}
                        placeholder={i18n.t('NUMBER_PLACEHOLDER')}
                    />
                    <TouchableOpacity style={{ width: '30%', alignSelf: "center", paddingTop: "10%" }} >
                        <Button label={i18n.t('BTN_SEND')} onPress={this.handleSendPress} />
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.WHITE,
        alignItems: "center",
        justifyContent: "space-between"
    },
    horizontalForm: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: "center",
        width: "100%"
    },
    logo: {
        flex: 0.7,
        width: "70%",
        resizeMode: "contain",
        alignSelf: "center"
    }, fullForm: {
        flex: 1,
        justifyContent: "center",
        width: "100%",
        padding: 20,
        bottom: 10
    },
    form: {
        flex: 1,
        width: "80%",
        paddingTop: "10%"
    },
    fullWidth: {
        alignContent: 'center',
        width: '50%',
        alignItems: "center",
        //  backgroundColor: colors.DODGER_BLUE,
        paddingHorizontal: 5,
        //  borderWidth: StyleSheet.hairlineWidth,
    },
    justify: {
        textAlign: 'center',
    }
});