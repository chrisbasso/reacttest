import React from "react";
import { Image, StyleSheet, View, KeyboardAvoidingView, TouchableOpacity, Text, Alert } from "react-native";
import Button from "../components/Button";
import PasswordInput from "../components/PasswordInput";
import FormNumericInput from "../components/FormNumericImput";
import imageLogo from "../assets/images/logo-galeno.png";
import colors from "../utils/config/colors";
import i18n from '../utils/language';
import { Dropdown } from 'react-native-material-dropdown';
import LoginController from '../controllers/LoginController'



interface State {
  number: string;
  password: string;
  passwordEncriptada: string;
  usuario: any;
}
class LoginScreen extends React.Component<{ navigation: any }, State> {
  readonly state: State = {
    number: "",
    password: "",
    passwordEncriptada: "",
    usuario: []
  };
  handleNumberChange = (email: string) => {
    this.setState({ number: email });
  };

  handlePasswordChange = (password: string) => {
    this.setState({ password: password });
  };

  handleLoginPress = () => {
    console.log("Login button pressed");
    if (this.state.password === "") {
      Alert.alert(i18n.t('MSG_MISSING_PASSWORD'))
      return;
    }
    if (this.state.number === "") {
      Alert.alert(i18n.t('MSG_MISSING_NUMBER'))
      return;
    }
    LoginController.encriptarContraseña(this.state.number, this.state.password, responseJson => {
      this.setState({
        usuario: responseJson
      })
      console.log(responseJson[0].nombre);
      Alert.alert(this.state.usuario[0].nombre);
      // this.props.navigation.navigate('Home');
    });




  };

  render() {
    let passwordInput
    let data = [
      {
        value: i18n.t('CREDENTIAL_NUMBER'),
      }, {
        value: i18n.t('DOCUMENT_NUMBER'),
      }];
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <Image source={imageLogo} style={styles.logo} />
        <View style={styles.form}>
          <Dropdown label={i18n.t('CREDENTIAL_TYPE')}  data={data} value={i18n.t('CREDENTIAL_NUMBER')} />
          <FormNumericInput label={i18n.t('NUMBER_PLACEHOLDER')}
            value={this.state.number}
            onChangeText={this.handleNumberChange}
            placeholder={i18n.t('NUMBER_PLACEHOLDER')}

            onSubmitEditing={() => { passwordInput.focus(); }}
          />
          <PasswordInput label={i18n.t('PASSWORD_PLACEHOLDER')}
            value={this.state.password}
            onChangeText={this.handlePasswordChange}
            placeholder={i18n.t('PASSWORD_PLACEHOLDER')}
            ref={(input) => passwordInput = input}
          />
          <Button label={i18n.t('LOGIN')} onPress={this.handleLoginPress} />
          <View style={styles.horizontalForm}>
            <TouchableOpacity style={styles.fullWidth} onPress={() => { this.props.navigation.navigate('ForgotPassword') }}><Text>{i18n.t('FORGOT_PASSWORD')}</Text></TouchableOpacity>
            <TouchableOpacity style={styles.fullWidth} onPress={() => { this.props.navigation.navigate('Signin') }}><Text>{i18n.t('REGISTER')}</Text></TouchableOpacity>
          </View>
          <View style={styles.fullForm}><Text style={styles.justify}>{i18n.t('MSG_LOGIN')}</Text></View>
        </View>
      </KeyboardAvoidingView >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
    alignItems: "center",
    justifyContent: "space-between"
  },
  horizontalForm: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: "center",
    width: "100%"
  },
  logo: {
    flex: 0.7,
    width: "70%",
    resizeMode: "contain",
    alignSelf: "center"
  }, fullForm: {
    flex: 1,
    justifyContent: "center",
    width: "100%",
    padding: 20,
    bottom: 10
  },
  form: {
    flex: 1,
    justifyContent: "center",
    width: "80%"
  },
  fullWidth: {
    alignContent: 'center',
    width: '50%',
    alignItems: "center",
    //  backgroundColor: colors.DODGER_BLUE,
    paddingHorizontal: 5,
    //  borderWidth: StyleSheet.hairlineWidth,
  },
  justify: {
    textAlign: 'center',
  }
});

export default LoginScreen;