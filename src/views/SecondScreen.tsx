import React from "react";
import { View, Text } from "react-native";
import i18n from '../utils/language';

export class SecondScreen extends React.Component <{navigation: any}, { }> {
  render() {

    const { navigation } = this.props;
    const item = navigation.getParam('item');

    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text style={{ fontSize : 30 }}>{item.codigo}</Text>
        <Text>{i18n.t('DETAILS_gender')}: {item.descripcion}</Text>
        <Text>{i18n.t('DETAILS_mass')}: {item.direccion}</Text>
        <Text>{i18n.t('DETAILS_height')}: {item.horario}</Text>
        <Text>{i18n.t('DETAILS_birth_year')}: {item.geoloc}</Text>
      </View>
    );
  }
}