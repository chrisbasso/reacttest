import React from "react";
import { View, Text, StyleSheet } from "react-native";
import ReactNativeItemSelect from 'react-native-item-select';


export default class HomeScreen extends React.Component <{navigation: any}, { loading: boolean, personaje: any[]}> {

  constructor(props : any){
    super(props);
    this.state = {
      loading: false,
      personaje: []
    };
  }

  getPersonajes = () => {

    this.setState({loading:true})
    
      fetch('http://gal-tomcat-mobile-test:8080/mobile-rest-services/vacunatorios/',{
          method: 'GET'
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          this.setState({
            personaje: responseJson,
            loading: false
          })
        })
        .catch((error) => {
          console.error(error);
        });
  }

  componentDidMount(){
    this.getPersonajes();
  }

  render() {
    if(this.state.loading){
      return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
          <Text>Cargando!</Text>
        </View>
      );
    }
    
    return (
      <View style={styles.row}>
        <ReactNativeItemSelect
            data = {this.state.personaje}
            itemComponent={
              (item: any) => (
                <View>
                    <Text>{item.descripcion}</Text>
                </View>
              )
            }
            onSubmit={(item: any) => this.props.navigation.navigate('Second', {item : item})}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20
  },
  box: {
    flex: 1,
    height: 100,
    backgroundColor: '#333',
  },
  box2: {
    backgroundColor: 'green'
  },
  box3: {
    backgroundColor: 'orange'
  },
  two: {
    flex: 2
  }
});