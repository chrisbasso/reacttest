import React from "react";
import { StyleSheet, View, Alert, ScrollView, KeyboardAvoidingView, Text } from "react-native";
import Button from "../components/Button";
import PasswordInput from "../components/PasswordInput";
import EmailInput from "../components/EmailInput";
import FormNumericInput from "../components/FormNumericImput";
import colors from "../utils/config/colors";
import i18n from '../utils/language';
import SigninController from "../controllers/SigninController";

interface State {
    number: string,
    password: string
    password2: string
    dni: string
    phone: string
    email: string
    email2: string
}

export default class SigninScreen extends React.Component<{ navigation: any }, State> {
    state = {
        number: "",
        password: "",
        password2: "",
        dni: "",
        phone: "",
        email: "",
        email2: "",
    };


    render() {
        let dniInput, passwordInput, phoneInput, ConfirmPasswordInput, emailInput, ConfirmEmailInput
        let handleSigninPress = () => {
            if (this.state.number.replace(".", "").replace("-", "").length < 9) {
                Alert.alert(i18n.t('MSG_MISSING_CREDENTIAL'))
                return;
            }
            if (this.state.dni.replace(".", "").replace("-", "").length < 7) {
                Alert.alert(i18n.t('MSG_MISSING_DOCUMENT'))
                return;
            }
            if (this.state.phone.replace(".", "").replace("-", "").length < 7) {
                Alert.alert(i18n.t('MSG_MISSING_CREDENTIAL'))
                return;
            }
            if (this.state.password !== this.state.password2) {
                Alert.alert(i18n.t('MSG_INCORRECT_PASSWORDS'))
                return;
            }
            if (this.state.email !== this.state.email2) {
                Alert.alert(i18n.t('MSG_INCORRECT_EMAILS'))
                return;
            }
            if (this.state.email.length < 1) {
                Alert.alert(i18n.t('MSG_MISSING_EMAIL'))
                return;
            }
            if (this.state.password.length < 1) {
                Alert.alert(i18n.t('MSG_MISSING_PASSWORD'))
                return;
            }
            SigninController.signin(() => { this.props.navigation.goBack() })

        }
        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                <Text style={{ textAlign: 'center', }}>{i18n.t('MSG_REGISTER')}</Text>
                <ScrollView style={{ flex: 1, width: '80%' }}>
                    <FormNumericInput
                        onSubmitEditing={() => { dniInput.focus(); }}
                        value={this.state.number}
                        onChangeText={(text) => this.setState({ number: text })}
                        placeholder={i18n.t('CREDENTIAL_NUMBER')} label={i18n.t('CREDENTIAL_NUMBER')}
                    />
                    <FormNumericInput ref={(input) => dniInput = input}
                        onSubmitEditing={() => { phoneInput.focus(); }}
                        value={this.state.dni}
                        onChangeText={(text) => this.setState({ dni: text })}
                        placeholder={i18n.t('DOCUMENT_NUMBER')} label={i18n.t('DOCUMENT_NUMBER')}
                    />
                    <FormNumericInput ref={(input) => phoneInput = input}
                        onSubmitEditing={() => { passwordInput.focus(); }}
                        value={this.state.phone}
                        onChangeText={(text) => this.setState({ phone: text })}
                        placeholder={i18n.t('PHONE_PLACEHOLDER')} label={i18n.t('PHONE_PLACEHOLDER')}
                    />
                    <PasswordInput ref={(input) => passwordInput = input}
                        onSubmitEditing={() => { ConfirmPasswordInput.focus(); }}
                        value={this.state.password}
                        onChangeText={(text) => this.setState({ password: text })}
                        placeholder={i18n.t('PASSWORD_PLACEHOLDER')} label={i18n.t('PASSWORD_PLACEHOLDER')}
                    />
                    <PasswordInput ref={(input) => ConfirmPasswordInput = input}
                        onSubmitEditing={() => { emailInput.focus(); }}
                        value={this.state.password2}
                        onChangeText={(text) => this.setState({ password2: text })}
                        placeholder={i18n.t('CONFIRM_PASSWORD_PLACEHOLDER')} label={i18n.t('CONFIRM_PASSWORD_PLACEHOLDER')}
                    />
                    <EmailInput ref={(input) => emailInput = input}
                        onSubmitEditing={() => { ConfirmEmailInput.focus(); }}
                        value={this.state.email}
                        onChangeText={(text) => this.setState({ email: text })}
                        placeholder={i18n.t('EMAIL_PLACEHOLDER')} label={i18n.t('EMAIL_PLACEHOLDER')}
                    />
                    <EmailInput ref={(input) => ConfirmEmailInput = input}
                        value={this.state.email2}
                        onChangeText={(text) => this.setState({ email2: text })}
                        placeholder={i18n.t('CONFIRM_EMAIL_PLACEHOLDER')} label={i18n.t('CONFIRM_EMAIL_PLACEHOLDER')}
                    />
                    <Button label={i18n.t('BTN_SIGNIN')} onPress={handleSigninPress} />
                </ScrollView>

            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.WHITE,
        alignItems: "center",
        justifyContent: "space-between"
    },
    scrollContainer: {
        // flex: 1,
        backgroundColor: colors.WHITE,
        // width: '100%',

    },
    horizontalForm: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: "center",
        width: "100%"
    },
    logo: {
        flex: 0.7,
        width: "70%",
        resizeMode: "contain",
        alignSelf: "center"
    }, fullForm: {
        flex: 1,
        justifyContent: "center",
        width: "100%",
        padding: 20,
        bottom: 10
    },
    form: {
        flex: 1,
        justifyContent: "center",
        width: "80%",
    },
    fullWidth: {
        alignContent: 'center',
        width: '50%',
        alignItems: "center",
        //  backgroundColor: colors.DODGER_BLUE,
        paddingHorizontal: 5,
        //  borderWidth: StyleSheet.hairlineWidth,
    },
    justify: {
        textAlign: 'center',
    }
});

