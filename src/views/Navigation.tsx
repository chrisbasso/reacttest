
import HomeScreen from "./HomeScreen";
import { SecondScreen } from "./SecondScreen";
import LoginScreen from "./LoginScreen";
import { createStackNavigator, createAppContainer } from "react-navigation";
import MapScreen from "./MapScreen";
import SigninScreen from "./SigninScreen";
import i18n from '../utils/language';
import ForgotPasswordScreen from "./ForgotPasswordScreen";


const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Second: {
      screen: SecondScreen,
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        header: null
      }
    },
    Map: {
      screen: MapScreen,
    },
    Signin: {
      screen: SigninScreen,
      navigationOptions: {
        title: i18n.t('REGISTER'),
      }
    },
    ForgotPassword: {
      screen: ForgotPasswordScreen,
      navigationOptions: {
        title: i18n.t('FORGOT_MY_PASSWORD'),
      }
    },
  }, {
    initialRouteName: 'Login',
  });

export default createAppContainer(AppNavigator);