module.exports = {
    preset: 'jest-expo',
    roots: [
        '<rootDir>/src'
    ],
    transform: {
        '^.+\\.js$': '<rootDir>/node_modules/react-native/jest/preprocessor.js',
        '.*\.tsx?$': 'ts-jest',
    },
    testMatch: [ 
        '**/__tests__/**/*.(js|ts|tsx)', 
        '**/?(*.)+(spec|test).(js|ts|tsx)', 
    ], 
    moduleFileExtensions: [
        'js',
        'jsx',
        'json',
        'ts',
        'tsx',
    ],
    modulePathIgnorePatterns: [
        "node_modules/*"
    ]
};